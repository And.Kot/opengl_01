#pragma once

#include <glm.hpp>

class Vertex
{
public:
	Vertex(const glm::vec3& pos);
	virtual ~Vertex();

private:
	glm::vec3 pos;
};

class Mesh
{
public:
	Mesh(Vertex* vertices, unsigned int numVertices);
	virtual ~Mesh();

	void Draw();

private:

};