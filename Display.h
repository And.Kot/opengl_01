#pragma once

#include <string>
#include <SDL.h>

class __declspec(dllexport) Display
{
public:
	Display(int width, int height, const std::string& title);
	virtual ~Display();

	bool IsClosed();
	void Update();
	void Paint();

protected:
private:
	SDL_Window* window;
	SDL_GLContext glContext;

	bool isClosed;
};


