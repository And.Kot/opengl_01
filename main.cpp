﻿#pragma once

#include <SDL.h>
#include "Display.h"
#include "Shader.h"
#include <iostream>

int main(int argc, char* args[])
{
	Display display(800, 600, "Hello World!");

	Shader shader("./resources/Shader");

	while (!display.IsClosed())
	{
		glClearColor(0.0f, 0.15f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		
		display.Update();
	}

	system("pause");
	return 0;
}
